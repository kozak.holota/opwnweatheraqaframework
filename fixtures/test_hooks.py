import os

import pandas


def pytest_addoption(parser):
    parser.addoption("--api-key",
                     action='store',
                     help="Open Weather API key")


def pytest_generate_tests(metafunc):
    params = tuple(
        pandas.read_csv(os.path.join("..", "test_data", f"{metafunc.function.__name__}.csv")).itertuples(index=False,
                                                                                                         name=None))

    if "current_weather_client" in metafunc.fixturenames:
        metafunc.parametrize("city, country_code", params)
