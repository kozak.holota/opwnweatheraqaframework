import pytest

from api_client.current_weather_client import CurrentWeatherClient


@pytest.fixture(scope="session")
def current_weather_client(request):
    api_client = CurrentWeatherClient(request.config.getoption("--api-key"))
    yield api_client
    del api_client
