import allure
import pytest

from api_client.current_weather_client import CurrentWeatherClient


@allure.suite("Current weather endpoint: /weather")
class TestCurrentWeather:
    @allure.title("Verify that current weather results for city and city with country are the same")
    @pytest.mark.city_country
    def test_current_weather_sync(self, current_weather_client: CurrentWeatherClient, city, country_code):
        single_city_request = current_weather_client.get_current_weather(city)
        city_country_request = current_weather_client.get_current_weather(city, country_code=country_code)

        assert single_city_request.ok, f"Request failed with the status code {single_city_request.status_code}"
        assert city_country_request.ok, f"Request failed with the status code {city_country_request.status_code}"

        assert single_city_request.json() == city_country_request.json(), f"""Requests with city and city with country give different results
        With city ({city}) only
        {single_city_request.json()}
        With city and country ({city}, {country_code})
        {city_country_request.json()}"""
