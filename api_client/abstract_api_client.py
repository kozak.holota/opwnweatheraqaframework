import json

import allure
import requests


class AbstractApiClient:
    def __init__(self, api_key):
        self.__api_key = api_key
        self.__client = requests.Session()

    @property
    def api_key(self):
        return self.__api_key

    def _get(self, endpoint, **kwargs):
        req_args = dict(kwargs)
        url = f"https://api.openweathermap.org/data/2.5/{endpoint}?{'&'.join([f'{i}={j}' for i, j in zip(req_args.keys(), req_args.values())])}&appid={self.api_key}"
        res = self.__client.get(url)
        allure.attach(json.dumps(res.json(), indent=4), "Request response", allure.attachment_type.JSON)
        return res

    def __del__(self):
        self.__client.close()