import allure

from api_client.abstract_api_client import AbstractApiClient


class CurrentWeatherClient(AbstractApiClient):

    @allure.step("Getting current weather for the city {1}")
    def get_current_weather(self, city, country_code=None, state_code=None):
        q = city
        if state_code:
            q += f",{state_code}"
        if country_code:
            q += f",{country_code}"

        return self._get('weather', q=q)
